const mysql = require('mysql')

//创建一个链接池，效率更高，不需要每次操作数据库都创建链接
const pool = mysql.createPool({
    // connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '123456',
    database: 'cms'
})

exports.query = function (sqlStr) {
    return new Promise((resolve, reject) => {
        //从链接池中拿一个链接出来
        pool.getConnection((err, connection) => {
            if (err) {
                return reject(err)
            }
            connection.query(sqlStr, (error, ...args) => {
                //使用完这个链接释放它
                connection.release()
                
                if (error) {
                    return reject(error)
                }
                resolve(...args)
            })
        })
    })
}