# cms

第三方包：

> - express
>
> - body-parser
>   - 处理请求体数据的
> - blueimp-md5
>   - 处理密码数据加密
> - moment
>   - 处理时间的
>   - node和浏览器都能使用
> - mysql
>   - 操作数据库



## 创建数据库

```sql
--- 内容管理系统

CREATE DATABASE IF NOT EXISTS cms;

use cms;

-- 用户表
CREATE TABLE users(
    id INT PRIMARY KEY auto_increment,
    username VARCHAR(50) NOT NULL, -- 用户名
    password VARCHAR(50) NOT NULL, -- 密码
    email VARCHAR(50) NOT NULL, -- 邮箱
    nickname VARCHAR(50) NOT NULL, -- 昵称
    avatar VARCHAR(100) NULL, -- 头像
    gender BIT NULL, -- 性别
    create_time DATETIME NOT NULL, -- 创建时间
    modify_time DATETIME NOT NULL -- 修改时间
);

-- 话题表
CREATE TABLE topics(
    id INT PRIMARY KEY auto_increment,
    title VARCHAR(100) NOT NULL, -- 文章标题
    content TEXT NOT NULL, -- 文章内容
    create_time DATETIME NOT NULL, -- 创建时间
    modify_time DATETIME NOT NULL, -- 修改时间
    user_id INT NOT NULL -- 所属用户
);

-- 评论表
CREATE TABLE comments(
    id INT PRIMARY KEY auto_increment,
    content TEXT NOT NULL, -- 评论内容
    create_time DATETIME NOT NULL, -- 创建时间
    modify_time DATETIME NOT NULL, -- 修改时间
    article_id INT NOT NULL, -- 所属文章
    user_id INT NOT NULL, -- 所属用户
    reply_id INT NULL -- 所属回复人
)
```

## 初始化项目结构

```
.
|-- README.md
|-- app.js
|-- config.js
|-- controllers
|-- models
|-- node_modules
|-- package-lock.json
|-- package.json
`-- router.js
```



## 接口设计

### 接口设计规范：RESTful

- [概念介绍](http://www.ruanyifeng.com/blog/2011/09/restful.html)
- [设计指南](http://www.ruanyifeng.com/blog/2014/05/restful_api.html)
- [实践](http://www.ruanyifeng.com/blog/2018/10/restful-api-best-practices.html)

###  接口设计

#### 用户资源处理

1. 获取所有用户

   请求方法：`GET`

   请求路径（url）：`/users`

   

2. 添加用户

   请求方法：`POST`

   请求路径（url）：`/users`

   请求体：

   - emial 邮箱
   - password 密码
   - nickname 昵称

   
   
3. 修改用户

   请求方法：`PATCH`

   请求路径（url）：`/users/:id`

   

4. 删除用户

   请求方法：`DELETE`

   请求路径（url）：`/users/:id`

   

#### 话题资源处理

1. 分页获取话题

   请求方法：`GET`

   请求路径（url）：`/topics`

   请求参数：

   - `_page`
     - 获取第几页
     - 默认值：1
   - `_limit`
     - 每页限制获取多少条
     - 默认值：20

   

2. 添加话题

   请求方法：`POST`

   请求路径（url）：`/topics`

   

3. 修改话题

   请求方法：`PATCH`

   请求路径（url）：`/topics/:id`

   - id 话题id

   

4. 删除话题

   请求方法：`DELETE`

   请求路径（url）：`/topics/:id`

   - id 话题id

   

#### 评论资源处理

1. 获取所有评论

   请求方法：`GET`

   请求路径（url）：`/comments`

   

2. 添加评论

   请求方法：`POST`

   请求路径（url）：`/comments`

   

3. 修改评论

   请求方法：`PATCH`

   请求路径（url）：`/comments/:id`

   - id 评论id

   

4. 删除评论

   请求方法：`DELETE`

   请求路径（url）：`/comments/:id`

   - id 评论id

   

#### 会话管理

1. 用户登录（创建会话）

   请求方法：`POST`

   请求路径（url）：`/session`

   

2. 用户推出（删除会话）

   请求方法：`DELETE`

   请求路径（url）：`/session`

   - id 评论id

   

3. 获取登录状态（获取会话状态）

   请求方法：`GET`

   请求路径（url）：`/session`

