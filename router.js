const express = require('express')
const db = require('./models/db')
const userController = require('./controllers/user')
const topicController = require('./controllers/topic')
const commentController = require('./controllers/comment')
const sessionController = require('./controllers/session')

const router = express.Router()

// 检查是否登录
function checkLogin(req, res, next) {
  if (!req.session.user) {
    return res.status(401).json({
      error: 'Unauthorized'
    })
  }
  next()
}

// 检查是否拥有话题的操作权限
async function checkTopic(req, res, next) {
  try {
    // 获取话题的 id
    const id = req.params.id
    const [topic] = await db.query(`SELECT * FROM topics WHERE id=${id}`)
    // 如果话题不存在
    if (!topic) {
      return res.status(404).json({
        error: 'Topic not found'
      })
    }
    // 如果话题不属于作者
    if (topic.user_id !== req.session.user.id) {
      return res.status(400).json({
        error: 'Invalid request'
      })
    }
    next()
  } catch (error) {
    next(error)
  }
}

// 检查是否拥有评论的操作权限
async function checkComment(req, res, next) {
  try {
    // 获取评论的 id
    const id = req.params.id
    const [comment] = await db.query(`SELECT * FROM comments WHERE id=${id}`)
    // 如果评论不存在
    if (!comment) {
      return res.status(404).json({
        error: 'Comment not found'
      })
    }
    // 如果评论不属于作者
    if (comment.user_id !== req.session.user.id) {
      return res.status(400).json({
        error: 'Invalid request'
      })
    }
    next()
  } catch (error) {
    next(error)
  }
}

// 检查是否拥有修改当前用户的操作权限
async function checkUser(req, res, next) {
  try {
    // 获取用户的 id
    const id = req.params.id
    const [user] = await db.query(`SELECT * FROM users WHERE id=${id}`)
    // 如果用户不存在
    if (!user) {
      return res.status(404).json({
        error: 'User not found'
      })
    }
    // 如果用户不属于操作者
    if (user.id !== req.session.user.id) {
      return res.status(400).json({
        error: 'Invalid request'
      })
    }
    next()
  } catch (error) {
    next(error)
  }
}

/**
 * 用户资源（users）
 */
router
  .get('/users/', userController.list)
  .post('/users/', userController.create)
  .patch('/users/:id', checkLogin, checkUser, userController.update)
  .delete('/users/:id', checkLogin, checkUser, userController.destroy)

/**
 * 话题资源（topics）
 */
router
  .get('/topics/', topicController.list)
  .get('/topics/:id', topicController.query)
  .post('/topics/', checkLogin, topicController.create)
  .patch('/topics/:id', checkLogin, checkTopic, topicController.update)
  .delete('/topics/:id', checkLogin, checkTopic, topicController.destroy)

/**
 * 评论资源（comments）
 */
router
  .get('/comments/', commentController.list)
  .post('/comments/', checkLogin, commentController.create)
  .patch('/comments/:id', checkLogin, checkComment, commentController.update)
  .delete('/comments/:id', checkLogin, checkComment, commentController.destroy)

/**
 * 会话资源（session）
 */
router
  .get('/session/', sessionController.get)
  .post('/session/', sessionController.create)
  .delete('/session/', sessionController.destroy)

//导出
module.exports = router