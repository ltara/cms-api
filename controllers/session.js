const md5 = require("blueimp-md5")
const db = require('../models/db')

/**
 * 获取会话状态：用户信息
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.get = (req, res, next) => {
    // 获取会话状态
    const {user} = req.session
    // 未登录状态，返回错误信息
    if (!user) {
        return res.status(401).json({
            error: 'Unauthorized'
        })
    }
    // 登录状态，发送响应
    res.status(200).json(user)
}

/**
 * 创建会话：用户登录
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.create = async (req, res, next) => {
    // 接受表单数据
    const body = req.body
    const password = md5(md5(body.password))
    
    // 操作数据库处理登录请求
    const sqlStr = `
        SELECT * FROM users WHERE email='${ body.email }' and password='${ password }'
    `
    try {
        const [user] = await db.query(sqlStr)
        if (!user) {
            res.status(404).json({
                error: 'Invalid email or password!'
            })
        }
        // 登录成功，记录 Session
        req.session.user = user
        // 发送响应
        res.status(201).json(user)
    } catch (error) {
        next(error)
    }
}

/**
 * 销毁会话：注销登录
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.destroy = (req, res, next) => {
    // 销毁会话
    delete req.session.user
    // 发送响应
    res.status(201).json({})
}
