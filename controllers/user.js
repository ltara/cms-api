const md5 = require('blueimp-md5')
const db = require('../models/db')
const sqlHelper = require('../utilities/sqlhelper')
/**
 * 获取用户信息列表
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.list = async (req, res, next) => {
  try {
    const andConditionStr = sqlHelper.andCondition(req.query)
    const sqlStr = `
      SELECT * FROM users WHERE ${ andConditionStr }
    `
    const user = await db.query(sqlStr)
    // 发送响应
    res.status(200).json(user)
  } catch (error) {
    next(error)
  }
}

/**
 * 添加用户
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.create = async (req, res, next) => {
  // try-catch处理报错信息
  try {
    // 操作数据库完成注册请求
    const body = req.body
    const sqlStr = `
      INSERT INTO users(username, password, email, nickname, avatar, gender)
      VALUES('${ body.email}',
        '${ md5(md5(body.password))}', 
        '${ body.email}',
        '${ body.nickname}',
        'default-avatar.jpg',
        0)
      `
    const ret = await db.query(sqlStr)
    const [users] = await db.query(`SELECT * FROM users WHERE id=${ret.insertId}`)
    res.status(201).json(users)
  } catch (error) {
    next(error)
  }
}

/**
 * 修改用户信息
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.update = async (req, res, next) => {
  try {
    // 用户的 id
    const id = req.params.id
    // 获取提交的表单数据
    const body = req.body
    // 修改基本信息
    if (body.nickname && body.gender) {
      const sqlBasicInfo = `UPDATE users 
        SET nickname='${ body.nickname }', gender=${ body.gender } WHERE id=${ id }
      `
      await db.query(sqlBasicInfo)
    }
    // 修改账号
    if (body.newPassword) {
      const [{password}] = await db.query(`SELECT * FROM users WHERE id=${ id }`)
      if (md5(md5(body.oldPassword)) == password) {
        const sqlAccountInfo = `
          UPDATE users SET password='${ md5(md5(body.newPassword)) }' WHERE id=${ id }
        `
        await db.query(sqlAccountInfo)
      } else {
        return res.status(422).json({})
      }
    }
    // 修改头像
    // const sqlheadportrait = `UPDATE users SET avatar='${ body.avatar }' WHERE id=${ id }`
    const [user] = await db.query(`SELECT * FROM users WHERE id=${ id }`)
    // 发送响应
    res.status(201).json(user)
  } catch (error) {
    next(error)
  }
}

/**
 * 删除用户
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.destroy = async (req, res, next) => {
  try {
    const id = req.params.id
    // 删除用户
    await db.query(`DELETE FROM users WHERE id=${id}`)
    // 删除用户发表的话题
    await db.query(`DELETE FROM topics WHERE user_id=${id}`)
    // 删除用户发表的评论
    await db.query(`DELETE FROM comments WHERE user_id=${id}`)
    // 发送响应
    res.status(201).json({})
  } catch (error) {
    next(error)
  }
}