const db = require('../models/db')

exports.list = async (req, res, next) => {
    try {
        const {topic_id} = req.query
        // 分页处理
        let {_page = 1, _limit = 10} = req.query

        if (_page < 1) {
            _page = 1
        }
        if (_limit < 1) {
            _limit = 1
        }
        if (_limit > 10) {
            _limit = 10
        }

        const start = (_page - 1) * _limit
        // 操作数据库
        const sqlStr = `
            SELECT * FROM comments WHERE topic_id=${ topic_id } LIMIT ${ start }, ${ _limit }
        `
        const [{
            count
        }] = await db.query(`SELECT COUNT(*) as count FROM comments WHERE topic_id=${ topic_id }`)
        const comments = await db.query(sqlStr)

        for (let i = 0; i < comments.length; i++) {
            const [{nickname}] = await db.query(`SELECT * FROM users WHERE id=${ comments[i].user_id }`)
            comments[i].nickname = nickname
        }
        // 发送响应
        res.status(200).json({
            comments,
            count
        })
    } catch (error) {
        next(error)
    }
}

exports.create = async (req, res, next) => {
    try {
        const {
            content = '',
            topic_id
        } = req.body
        const sqlStr = `
        INSERT INTO comments(content, topic_id, user_id)
        VALUES('${ content }',
            ${ topic_id },
            ${ req.session.user.id })
        `
        const ret = await db.query(sqlStr)
        const [comment] = await db.query(`SELECT * FROM comments WHERE id=${ ret.insertId }`)
        res.status(201).json(comment)
    } catch (error) {
        next(error)
    }
}

exports.update = async (req, res, next) => {
    try {
        // 话题的 id
        const id = req.params.id
        // 获取提交的表单数据
        const body = req.body
        // 操作数据库
        const sqlStr = `
            UPDATE comments 
            SET content='${ body.content }', topic_id='${ body.topic_id }'
            WHERE id=${ id }
        `
        await db.query(sqlStr)
        const [comment] = await db.query(`SELECT * FROM comments WHERE id=${ id }`)
        // 发送响应
        res.status(201).json(comment)
    } catch (error) {
        next(error)
    }
}

exports.destroy = async (req, res, next) => {
    try {
        const id = req.params.id
        // 操作数据库
        await db.query(`DELETE FROM comments WHERE id=${ id }`)
        // 发送响应
        res.status(201).json({})
    } catch (error) {
        next(error)
    }
}