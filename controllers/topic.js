const db = require('../models/db')

/**
 * 获取话题列表
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.list = async (req, res, next) => {
    try {
        // 分页处理
        let {
            _page = 1, _limit = 10
        } = req.query

        if (_page < 1) {
            _page = 1
        }
        if (_limit < 1) {
            _limit = 1
        }
        if (_limit > 10) {
            _limit = 10
        }

        const start = (_page - 1) * _limit

        // 操作数据库
        const sqlStr = `
            SELECT * FROM topics LIMIT ${ start }, ${ _limit }
        `
        const [{
            count
        }] = await db.query('SELECT COUNT(*) as count FROM topics')
        const topics = await db.query(sqlStr)

        for (let i = 0; i < topics.length; i++) {
            const [{
                nickname: topicNickname
            }] = await db.query(`SELECT * FROM users WHERE id=${ topics[i].user_id }`)
            
            const comments = await db.query(`SELECT * FROM comments WHERE topic_id=${ topics[i].id }`)
            
            if (comments.length) {
                var [{nickname: commentNickname}] = await db.query(`SELECT * FROM users WHERE id=${ comments[comments.length - 1].user_id }`)
            }
            
            // 给返回的话题添加话题作者的昵称
            topics[i].nickname = topicNickname

            // 给返回的话题添加评论信息
            topics[i].comments = {
                count: comments.length,
                lastUser: comments.length ? `${ commentNickname } 回答了问题` : '还没有人回答问题'
            }
        }

        // 发送响应
        res.status(200).json({
            topics,
            count
        })
    } catch (error) {
        next(error)
    }
}

/**
 * 获取话题
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.query = async (req, res, next) => {
    try {
        const id = req.params.id
        // 查询话题
        const sqlStr = `
            SELECT * FROM topics WHERE id=${ id }
        `

        const topic = await db.query(sqlStr)
        // 话题浏览量加 1
        await db.query(`UPDATE topics SET page_view = page_view + 1 WHERE id=${ id }`)

        const [{
            nickname
        }] = await db.query(`SELECT * FROM users WHERE id=${ topic[0].user_id }`)
        topic[0].nickname = nickname
        // 发送响应
        res.status(200).json(topic[0])
    } catch (error) {
        next(error)
    }
}

/**
 * 创建话题
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.create = async (req, res, next) => {
    try {
        // 获取提交表单数据
        const body = req.body
        body.user_id = req.session.user.id
        // 操作数据库
        const sqlStr = `
            INSERT INTO topics(title, content, user_id, page_view, module)
            VALUES('${ body.title }',
            '${ body.content }',
            '${ body.user_id }',
            0,
            '${ body.module }')
        `
        const ret = await db.query(sqlStr)
        const [topic] = await db.query(`SELECT * FROM topics WHERE id='${ ret.insertId }'`)
        // 发送响应
        res.status(201).json(topic)
    } catch (error) {
        next(error)
    }
}

/**
 * 更新话题
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.update = async (req, res, next) => {
    try {
        // 话题的 id
        const id = req.params.id
        // 获取提交的表单数据
        const body = req.body
        // 操作数据库
        const sqlStr = `
            UPDATE topics 
            SET title='${ body.title }', content='${ body.content }'
            WHERE id=${ id }
        `
        await db.query(sqlStr)
        const [topic] = await db.query(`SELECT * FROM topics WHERE id=${ id }`)
        // 发送响应
        res.status(201).json(topic)
    } catch (error) {
        next(error)
    }
}

/**
 * 删除话题
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.destroy = async (req, res, next) => {
    try {
        const id = req.params.id
        // 操作数据库
        await db.query(`DELETE FROM topics WHERE id=${ id }`)
        // 发送响应
        res.status(201).json({})
    } catch (error) {
        next(error)
    }
}