const express = require('express')
const router = require('./router')
const bodyParser = require('body-parser')
const session = require('express-session')

const app = express()

// 允许跨域访问(前端访问后端)
// app.all('*', function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "X-Requested-With");
//     res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
//     res.header("X-Powered-By",' 3.2.1')
//     res.header("Content-Type", "application/json;charset=utf-8");
//     next();
// })

/**
 * 配置表单请求体
 * parse application/x-www-form-urlencoded
 */
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())

/**
 * 配置使用 session 中间件
 */
app.use(session({
    secret: 'ltara',
    resave: false,
    saveUninitialized: false
    // cookie: {
    //     secure: true
    // }
}))

// 路由
app.use(router)

// 统一处理错误
app.use((err, req, res, next) => {
    res.status(500).json({
        error: err.message
    })
})

// 开启监听端口
app.listen(3000, () => {
    console.log('app is running at port 3000')
    console.log('Please visit http://127.0.0.1:3000/')
})